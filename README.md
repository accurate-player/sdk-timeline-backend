# SDK Timeline Backend

The [timeline component](https://gitlab.com/accurate-player/accurate-player-examples/-/tree/master/sdk/timeline), 
available as part of the [Accurate Player SDK package](https://accurate.video/products/accurate-player-sdk/), can show thumbnails and audio-volume information, called audio 
waveforms. This repository explains how to generate thumbnails & sprite maps, and extract the required audio 
information on the backend, using free tools such as FFmpeg & ImageMagick.

# Prerequisites

The following free tools needs to be installed on the backend. Refer to the installation instructions for each tool for 
more details.

* [FFmpeg](https://ffmpeg.org/)
* [ImageMagick montage](https://imagemagick.org/script/index.php)
* [BBC Audiowaveform](https://github.com/bbc/audiowaveform)
* [Python 3 runtime](https://www.python.org/)

# Audio waveforms

Refer to the [waveforms documentation](https://gitlab.com/accurate-player/accurate-player-examples/-/blob/master/sdk/timeline/docs/WAVEFORMS.md) in the Accurate Player SDK docs for more details.

Example:

```
# first transcode audio into wav/pcm
ffmpeg -i Bunny_H264_Webproxy_TC2.mp4 Bunny_H264_Webproxy_TC2.wav

# generate waveform, 8-bit resolution, 128 input samples per data point
audiowaveform -i Bunny_H264_Webproxy_TC2.wav -z 128 -b 8 -o waveformdata.dat
```

# Thumbnails

Refer to the [thumbnail documentation](https://gitlab.com/accurate-player/accurate-player-examples/-/blob/master/sdk/timeline/docs/THUMBNAILS.md) in the Accurate Player SDK docs for more details.

Usage:

```
usage: spritemap.py [-h] -n FPS_NUMERATOR -d FPS_DENOMINATOR -W IMAGE_WIDTH [-H IMAGE_HEIGHT] -i INTERVAL [-om OUTPUT_MANIFEST] [-os OUTPUT_SPRITEMAP] VIDEO_FILE
```

Example:

```
./spritemap.py -n 24 -d 1 -i 25 -W 320 -H 240 ~/Videos/Bunny_H264_Webproxy_TC2.mp4
```